﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yoshimoshi.Machines.GCode;

namespace Yoshimoshi.Machines.Yoshibot
{
    /// <summary>
    /// Provides execution logic using the default Yoshibot Arduino controller code
    /// </summary>
    public class YoshibotExecutor : Executor
    {
        private YoshibotBase m_robot;
        private Movement m_movement = null;

        public YoshibotExecutor(YoshibotBase robot)
        {
            m_robot = robot;
        }

        protected override async Task BeforeExecuteBlock()
        {
            await base.BeforeExecuteBlock();
            m_movement = null;
        }

        protected override async Task AfterExecuteBlock()
        {
            await base.AfterExecuteBlock();

            // physically move the robot
            if (m_movement != null)
            {
                if (await m_robot.Move(m_movement))
                {
                    // TODO: update for all axes
                    var newPos = new Position()
                    {
                        A = m_robot.Axes[0].CurrentPosition,
                        B = m_robot.Axes[1].CurrentPosition,
                        C = m_robot.Axes[2].CurrentPosition,
                        D = m_robot.Axes[3].CurrentPosition,
                        E = m_robot.Axes[4].CurrentPosition,
                        F = m_robot.Axes[5].CurrentPosition,
                    };
                    CommandedPosition = newPos;
                }
            }
        }

        protected override async Task<TimeSpan> ExecuteCommand(Command command)
        {
            // multiple axis moves may be in a single block
            // we'll just calculate all of the moves we need to send to the robot between before and after, 
            // then send during the "after"
            if (command is AxisMoveCommand)
            {
                var amc = command as AxisMoveCommand;

                if (m_movement == null)
                {
                    m_movement = new Movement(CurrentPositionType);
                }

                var targetAxis = m_robot.Axes.FirstOrDefault(a => a.AxisIdentifier == amc.AxisIdentifier);
                if (targetAxis == null)
                {
                    throw new Exception($"Unknown axis {amc.AxisIdentifier}");
                }

                var move = new AxisMove(targetAxis, amc.Position);
                m_movement.Moves.Add(move);

                // we took no time to determine the move, actual move time will get added by the AfterExecuteBlock
                return await Task.FromResult(TimeSpan.Zero);
            }
            else if (command is FeedrateCommand)
            {
                base.FeedRate = (command as FeedrateCommand).Rate;
                return await Task.FromResult(TimeSpan.Zero);
            }
            else
            {
                return await base.ExecuteCommand(command);
            }
        }
    }

    /// <summary>
    /// Represents the 6-DOF Yoshibot 1
    /// </summary>
    public class Yoshibot1 : YoshibotBase
    {
        public Yoshibot1(string serialPort)
            : base(serialPort)
        {
        }
    }

    public abstract class YoshibotBase : Robot, IDisposable
    {
        public const int DefaultBaudRate = 9600;
        public const int DefaultDefinedJoints = 6;
        public const int DefaultMovementSpeedDelay = 0;
        public const int MaxJoints = 16;

        protected const int JointIndexBase = 0;
        protected const int JointIndexShoulder = 1;
        protected const int JointIndexElbow = 2;
        protected const int JointIndexWristRotate = 3;
        protected const int JointIndexWristFlex = 4;
        protected const int JointIndexGripper = 5;

        private SerialPort m_port;
        private int[] m_knownPositions;
        private int m_delay;
        private StringBuilder m_commandBuilder = new StringBuilder();
        private int m_feedrate = 100;
        private const int DelayMsPerPercentagePoint = 2;

        /*
        #define SERVO_CENTER      300  
        #define SERVO_MAX_CW      110  
        #define SERVO_MAX_CCW     490 
        */
        public override bool IsMoving { get; protected set; }

        public YoshibotBase(string portName, int degreesOfFreedom = DefaultDefinedJoints)
            : base(degreesOfFreedom)
        {
            if (degreesOfFreedom > MaxJoints) throw new ArgumentException($"The Yoshibot controller only supports up to {MaxJoints} servos");

            m_port = new SerialPort(portName, DefaultBaudRate);
            m_knownPositions = new int[degreesOfFreedom];
            m_delay = DefaultMovementSpeedDelay;

            var axes = new Axis[degreesOfFreedom];

            var id = AxisIdentifier.A;

            for (int i = 0; i < degreesOfFreedom; i++)
            {
                axes[i] = new Axis($"J{i}", id++, 110, 490, 300);
                m_knownPositions[i] = (int)axes[i].CurrentPosition;
            }

            Axes = new AxisCollection(axes);
        }

        public void Dispose()
        {
            m_port.Dispose();
        }

        public async override Task<bool> Move(Movement movement)
        {
            if (IsMoving)
            {
                Debug.WriteLine($"Move in progress");
                return false;
            }

            IsMoving = true;

            try
            {
                if (!m_port.IsOpen)
                {
                    m_port.ReadTimeout = 50;
                    m_port.WriteTimeout = 50;
                    m_port.Open();
                }

                var commands = GenerateMovementCommands(movement);

                if (commands == null || commands.Length == 0)
                {
                    // nothing to do
                    return true;
                }

                var moveSuccess = true;

                foreach (var command in commands)
                {
                    moveSuccess = await Task.Factory.StartNew(() =>
                    {
                        Debug.Write($"TX:{command}");
                        try
                        {
                            m_port.Write(command);

                            // the yoshibot controller will send back an "ok" when the command is done executing
                            var response = m_port.ReadLine();
                            Debug.WriteLine($"RX:{response}");

                            return true;
                        }
                        catch (TimeoutException)
                        {
                            Debug.WriteLine($"Port locked.  Resetting...");
                            Task.Delay(500);
                            m_port.Close();
                            Task.Delay(500);
                            m_port.Open();
                            return false;
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine($"Write error: { ex.Message}");
                            return false;
                        }
                    });

                    Debug.WriteLine($"POST RX");

                    if (moveSuccess)
                    {
                        var maxChangeDegrees = 0d;

                        // Copy the movement values to the known positions
                        for (int i = 0; i < movement.Moves.Count; i++)
                        {
                            var delta = Math.Abs(m_knownPositions[i] - (int)movement.Moves[i].NewPosition);
                            if (delta > maxChangeDegrees) maxChangeDegrees = delta;

                            m_knownPositions[i] = (int)movement.Moves[i].NewPosition;
                        }
                    }
                }

                return moveSuccess;
            }
            finally
            {
                AfterMoveCompleted(movement);
                IsMoving = false;
            }
        }

        public int Feedrate
        {
            get => m_feedrate;
            set
            {
                if (Feedrate == value) return;
                if ((value > 100) || (value < 1)) return;

                // this is a "percentage" value.  The way the driver (arduino code) itself works is by introducing delays to movements to slow itself.  
                // 100% rate means zero delay introduced.  We'll arbitraily call each percentage point being [X]ms delay added.
                m_delay = (int)((double)(100 - value) * 0.5);
                m_feedrate = value;
            }
        }

        public const int MaxSingleJointMove = 10;

        protected virtual string[] GenerateMovementCommands(Movement movement)
        {
            // the yoshibot takes in moves in the format:
            //  [joint]|[speed]|position\n

            var commands = new List<string>();

            if (movement.Feedrate.HasValue)
            {
                Feedrate = movement.Feedrate.Value;
            }

            var jointIndexes = new List<int>();
            var currentPositions = new List<int>();
            var distanceToGo = new List<int>();
            var distancePerStep = new List<int>();

            var smallestMove = 1000; // arbitrarily larger than any possible move the bot allows

            // determine which joints move and how much, then just interpolate a "linear" movement of all of them
            for (int i = 0; i < m_knownPositions.Length; i++)
            {
                var request = movement.Moves.FirstOrDefault(m => m.Axis.Name == $"J{i}");
                if (request != null)
                {
                    if (request.NewPosition != m_knownPositions[i])
                    {
                        jointIndexes.Add(i);

                        // this joint is being asked to move - how far?
                        var moveAmount = (int)(request.NewPosition - m_knownPositions[i]);
                        distanceToGo.Add(moveAmount);

                        // look for the smallest absolute move
                        if (Math.Abs(moveAmount) < smallestMove)
                        {
                            smallestMove = Math.Abs(moveAmount);
                        }

                        // record the joint start position
                        currentPositions.Add(m_knownPositions[i]);
                    }
                }
            }

            if (currentPositions.Count == 0)
            {
                // no actual moves were requested
                return new string[0];
            }

            // divide each of the moves into MaxSingleJointMove's or smaller (this is again arbitrary)
            var totalSteps = (smallestMove / MaxSingleJointMove);
            if (smallestMove % MaxSingleJointMove != 0) totalSteps++;

            // we now know how many iterations it will take to achieve the movement, now figure out the amount of each
            foreach (var m in distanceToGo)
            {
                distancePerStep.Add(m / totalSteps);
            }

            // now calculate the actual absolute moves
            for (int i = 0; i < totalSteps ; i++)
            {
                for(int listIndex = 0; listIndex < jointIndexes.Count; listIndex++)
                {
                    var delta = 0;
                    // moving in the positive direction (CCW)
                    if (distanceToGo[listIndex] >  0)
                    {
                        if (distanceToGo[listIndex] > distancePerStep[listIndex])
                        {
                            delta = distancePerStep[listIndex];
                        }
                        else
                        {
                            delta = distanceToGo[listIndex];
                        }
                    }
                    else
                    {
                        // moving inthe negative direction (CW)
                        if (distanceToGo[listIndex] < distancePerStep[listIndex])
                        {
                            delta = distancePerStep[listIndex];
                        }
                        else
                        {
                            delta = distanceToGo[listIndex];
                        }
                    }

                    distanceToGo[listIndex] -= delta;
                    currentPositions[listIndex] += delta;
                    commands.Add($"{jointIndexes[listIndex]}|{movement.Feedrate}|{currentPositions[listIndex]}\n");
                }
            }

            return commands.ToArray();
        }

        private object List<T>()
        {
            throw new NotImplementedException();
        }
    }
}
