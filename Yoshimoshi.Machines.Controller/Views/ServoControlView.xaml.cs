﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yoshimoshi.Machines.Controller.ViewModels;

namespace Yoshimoshi.Machines.Controller.Views
{
    /// <summary>
    /// Interaction logic for ServoControl.xaml
    /// </summary>
    public partial class ServoControlView : Window
    {
        public ServoControlView()
        {
            this.DataContext = new ServoControlViewModel();

            InitializeComponent();
        }
    }
}
