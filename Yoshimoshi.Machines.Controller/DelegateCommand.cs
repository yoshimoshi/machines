﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace Yoshimoshi.Machines.Controller
{
    public class DelegateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action m_executeAction;

        public DelegateCommand(Action executeAction)
        {
            m_executeAction = executeAction;
        }

        public void Execute(object parameter) => m_executeAction();

        public bool CanExecute(object parameter) => true;
    }

    public class DelegateCommand<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action<T> m_executeAction;

        public DelegateCommand(Action<T> executeAction)
        {
            m_executeAction = executeAction;
        }

        public void Execute(object parameter) => m_executeAction((T)parameter);

        public bool CanExecute(object parameter) => true;
    }

    public class InvertBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }
}
