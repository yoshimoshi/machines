﻿using System.Collections.Generic;
using System.Threading;
using Yoshimoshi.Machines.LittleArm;

namespace Yoshimoshi.Machines.Controller.ViewModels
{
    class ServoControlViewModel : ViewModelBase
    {
        private Big m_servo;
        private AutoResetEvent m_moveEvent = new AutoResetEvent(false);
        private Queue<Movement> m_moveRequests = new Queue<Movement>();

        private string m_selectedServo;
        private double m_currentValue;

        public ServoControlViewModel()
        {
            m_servo = new Big("COM19");
            AvailableServos = new string[] { "S1" };
            SelectedServo = AvailableServos[0];

            CurrentValue = 0;

            new Thread(new ThreadStart(MoveThreadProc))
            {
                IsBackground = true
            }.Start();
        }

        public string[] AvailableServos { get; }

        public string SelectedServo
        {
            get => m_selectedServo;
            set => SetProperty(ref m_selectedServo, value);
        }

        public int CurrentValue
        {
            get => (int)m_servo.Axes[0].CurrentPosition;
            set
            {
                SetProperty(ref m_currentValue, value);

                // moves are async and slow, queue them of processing outside of the property
                var movement = new Movement(PositionType.Absolute, new AxisMove(m_servo.Axes[0], m_currentValue));
                m_moveRequests.Enqueue(movement);
                m_moveEvent.Set();
            }
        }

        private async void MoveThreadProc()
        {
            while (true)
            {
                if (m_moveEvent.WaitOne(1000))
                {
                    while (m_moveRequests.Count > 0)
                    {
                        var movement = m_moveRequests.Dequeue();
                        await m_servo.Move(movement);
                        RaisePropertyChanged(nameof(CurrentValue));
                    }
                }
            }
        }
    }
}
