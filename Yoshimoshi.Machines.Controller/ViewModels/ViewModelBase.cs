﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace Yoshimoshi.Machines.Controller.ViewModels
{
    class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool m_busy = false;

        public bool IsBusy
        {
            get => m_busy;
            set => SetProperty(ref m_busy, value);
        }

        protected void SetProperty<T>(ref T storageVariable, T value, bool alwaysRaiseEvent = false, [CallerMemberName] string propertyName = null)
        {
            // has the value changed (and we're not always raising the event)
            if (!alwaysRaiseEvent && EqualityComparer<T>.Default.Equals(storageVariable, value))
            {
                return;
            }

            // store the new value
            storageVariable = value;
            RaisePropertyChanged(propertyName);
        }

        protected void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanged<TProperty>(Expression<Func<TProperty>> property)
        {
            var me = (MemberExpression)property.Body;
            RaisePropertyChanged(me.Member.Name);
        }
    }
}
