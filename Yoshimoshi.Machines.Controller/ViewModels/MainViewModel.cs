﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Yoshimoshi.Machines.Controller.Views;
using Yoshimoshi.Machines.GCode;

namespace Yoshimoshi.Machines.Controller.ViewModels
{
    class MainViewModel : ViewModelBase
    {
        public const int JointsSupported = 6;

        private Yoshibot.Yoshibot1 m_robot;
        private Executor m_executor;
        private AutoResetEvent m_moveEvent = new AutoResetEvent(false);
        private Queue<Movement> m_moveRequests = new Queue<Movement>();
        private int[] m_currentPositions;
        private int[] m_maxPositions;
        private int[] m_minPositions;
        private int m_feedrate;
        private int m_lastFeedrate;
        private bool m_positionSynchronized = false;
        private bool m_liveMove;
        private string[] m_allPorts;
        private string m_selectedPort;
        private bool m_connected;
        private int m_dwell = 1000;
        private string m_programName = "Yoshimoshi Robot Move";
        private int m_startLineNumber = 100;
        private NCGenerator m_generator = new NCGenerator();
        private string m_currentProgram;
        private double[] m_lastRecordedPosition = new double[JointsSupported];

        public MainViewModel()
        {
            m_currentPositions = new int[JointsSupported];
            m_maxPositions = new int[JointsSupported];
            m_minPositions = new int[JointsSupported];

            AllSerialPorts = System.IO.Ports.SerialPort.GetPortNames();

            new Thread(new ThreadStart(MoveThreadProc))
            {
                IsBackground = true
            }.Start();
        }

        public string[] AllSerialPorts
        {
            get => m_allPorts;
            set => SetProperty(ref m_allPorts, value);
        }

        public string SelectedSerialPort
        {
            get => m_selectedPort;
            set
            {
                if (m_robot != null)
                {
                    m_robot.Dispose();
                }

                m_robot = new Yoshibot.Yoshibot1(value);
                m_executor = new Yoshibot.YoshibotExecutor(m_robot);

                SetProperty(ref m_selectedPort, value);
                Connected = true;
                GoHome();
            }
        }

        public bool Connected
        {
            get => m_connected;
            set => SetProperty(ref m_connected, value);
        }

        public ICommand ServoControlCommand
        {
            get => new DelegateCommand(() =>
                {
                    var view = new ServoControlView();
                    view.ShowDialog();
                });
        }

        public int J1MaxValue
        {
            get => m_maxPositions[0];
            set => SetProperty(ref m_maxPositions[0], value);
        }

        public int J1MinValue
        {
            get => m_minPositions[0];
            set => SetProperty(ref m_minPositions[0], value);
        }

        public int J1CurrentValue
        {
            get => m_currentPositions[0];
            set
            {
                SetProperty(ref m_currentPositions[0], value);
                if (LiveMove)
                {
                    MoveNow();
                }
            }
        }

        public int J2CurrentValue
        {
            get => m_currentPositions[1];
            set
            {
                SetProperty(ref m_currentPositions[1], value);
                if (LiveMove)
                {
                    MoveNow();
                }
            }
        }

        public int J3CurrentValue
        {
            get => m_currentPositions[2];
            set
            {
                SetProperty(ref m_currentPositions[2], value);
                if (LiveMove)
                {
                    MoveNow();
                }
            }
        }

        public int J4CurrentValue
        {
            get => m_currentPositions[3];
            set
            {
                SetProperty(ref m_currentPositions[3], value);
                if (LiveMove)
                {
                    MoveNow();
                }
            }
        }

        public int J5CurrentValue
        {
            get => m_currentPositions[4];
            set
            {
                SetProperty(ref m_currentPositions[4], value);
                if (LiveMove)
                {
                    MoveNow();
                }
            }
        }

        public int J6CurrentValue
        {
            get => m_currentPositions[5];
            set
            {
                SetProperty(ref m_currentPositions[5], value);
                if (LiveMove)
                {
                    MoveNow();
                }
            }
        }

        public int FeedrateCurrentValue
        {
            get => m_feedrate;
            set => SetProperty(ref m_feedrate, value);
        }

        public bool LiveMove
        {
            get => m_liveMove;
            set => SetProperty(ref m_liveMove, value);
        }

        public ICommand MoveToPositionCommand
        {
            get => new DelegateCommand(() =>
            {
                MoveNow();
            });
        }

        public ICommand HomeCommand
        {
            get => new DelegateCommand(() =>
            {
                GoHome();
            });
        }

        private void GoHome()
        {
            // TODO: get these from the actual robot instance
            J1MinValue = (int)m_robot.Joints[0].MinPosition.Value;
            J1MaxValue = (int)m_robot.Joints[0].MaxPosition.Value;
            J1CurrentValue = (int)m_robot.Joints[0].CurrentPosition;
            J2CurrentValue = (int)m_robot.Joints[1].CurrentPosition;
            J3CurrentValue = (int)m_robot.Joints[2].CurrentPosition;
            J4CurrentValue = (int)m_robot.Joints[3].CurrentPosition;
            J5CurrentValue = (int)m_robot.Joints[4].CurrentPosition;
            J6CurrentValue = (int)m_robot.Joints[5].CurrentPosition;

            FeedrateCurrentValue = 100;
        }

        private void MoveNow()
        {
            if (!Connected) return;

            var movement = GetMovementForCurrentPosition(PositionType.Absolute);
            movement.Feedrate = FeedrateCurrentValue;

            // moves are async and slow, queue them of processing outside of the property
            m_moveRequests.Enqueue(movement);
            m_moveEvent.Set();
        }

        public bool PositionSynchronized
        {
            get => m_positionSynchronized;
            set => SetProperty(ref m_positionSynchronized, value);
        }

        private async void MoveThreadProc()
        {
            while (true)
            {
                if (m_moveEvent.WaitOne(1000))
                {
                    while (m_moveRequests.Count > 0)
                    {
                        var movement = m_moveRequests.Dequeue();
                        await m_robot.Move(movement);
                    }

                    RaisePropertyChanged(nameof(PositionSynchronized));
                }
            }
        }

        private Movement GetMovementForCurrentPosition(PositionType moveType)
        {
            var movement = new Movement(PositionType.Absolute);

            for (int i = 0; i < 6; i++)
            {
                movement.Moves.Add(new AxisMove(m_robot.Axes[i], m_currentPositions[i]));
            }

            return movement;
        }

        public string CurrentProgram
        {
            get => m_currentProgram;
            set => SetProperty(ref m_currentProgram, value);
        }

        public int Dwell
        {
            get => m_dwell;
            set
            {
                if (value > 0)
                {
                    SetProperty(ref m_dwell, value);
                }
            }
        }

        public string ProgramName
        {
            get => m_programName;
            set => SetProperty(ref m_programName, value);
        }

        public int StartLineNumber
        {
            get => m_startLineNumber;
            set => SetProperty(ref m_startLineNumber, value);
        }

        public ICommand RecordPositionCommand
        {
            get => new DelegateCommand(() =>
            {
                if (m_lastFeedrate != FeedrateCurrentValue)
                {
                    m_generator.AddFeedrate(FeedrateCurrentValue);
                    m_lastFeedrate = FeedrateCurrentValue;
                }

                // TODO: add support for relative positioning
                var movement = GetMovementForCurrentPosition(PositionType.Absolute);

                // TODO: add support for fast or rotational moves
                m_generator.AddMove(MotionType.LinearMove, movement);

                // copy current position to last recorded
                for (var i = 0; i < m_currentPositions.Length; i++)
                {
                    m_lastRecordedPosition[i] = m_currentPositions[i];
                }

                CurrentProgram = m_generator.GetNCCode(StartLineNumber, ProgramName);
            });
        }

        public ICommand ClearRecordingCommand
        {
            get => new DelegateCommand(() =>
            {
                // TODO: add a speed bump/confirmation
                m_generator.Start();
                CurrentProgram = string.Empty;
            });
        }

        public ICommand AddDwellCommand
        {
            get => new DelegateCommand(() =>
            {
                if (Dwell > 0)
                {
                    m_generator.AddDwell(Dwell);
                    CurrentProgram = m_generator.GetNCCode(StartLineNumber, ProgramName);
                }
            });
        }

        public ICommand SaveProgramCommand
        {
            get => new DelegateCommand(() =>
            {
                var sfd = new SaveFileDialog();
                sfd.Title = "Save GCode";
                sfd.Filter = "GCode (*.gcode)|*.gcode|All files (*.*)|*.*";
                sfd.DefaultExt = "*.gcode";
                sfd.AddExtension = true;

                var result = sfd.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    var fi = new FileInfo(sfd.FileName);
                    var doSave = true;
                    if (fi.Exists)
                    {
                        if (MessageBox.Show($"Do you want to overwrite '{fi.Name}'?", "Overwrite", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.No)
                        {
                            doSave = false;
                        }
                    }

                    if (doSave)
                    {
                        using (var writer = fi.CreateText())
                        {
                            writer.Write(this.CurrentProgram);

                            // TODO update program name, which should update the Form title and probably the GCODE comments

                            // TODO: add a status bar to the form and put the save status there
                        }

                        // TODO: add exception handling
                    }
                }
            });
        }

        public ICommand LoadProgramCommand
        {
            get => new DelegateCommand(() =>
            {
                // TODO: 
                var ofd = new OpenFileDialog();
                ofd.Title = "Open GCode";
                ofd.DefaultExt = "*.gcode";
                ofd.AddExtension = true;
                ofd.Filter = "GCode (*.gcode)|*.gcode|All files (*.*)|*.*";
                ofd.Multiselect = false;
                ofd.CheckFileExists = true;

                var result = ofd.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    // TODO: 
                    var fi = new FileInfo(ofd.FileName);
                    if (fi.Exists)
                    {
                        using (var reader = fi.OpenText())
                        {
                            CurrentProgram = reader.ReadToEnd();
                            // TODO update program name, which should update the Form title and probably the GCODE comments

                            // TODO: add a status bar to the form and put the save status there
                        }
                        // TODO: add exception handling
                    }
                }
            });
        }

        public ICommand RunProgramCommand
        {
            get => new DelegateCommand(async () =>
            {
                await m_executor.Run(new NCReader(CurrentProgram));
                // TODO:
            });
        }
    }
}
