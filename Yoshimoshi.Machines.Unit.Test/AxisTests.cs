using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Yoshimoshi.Machines.Unit.Test
{
    [TestClass]
    public class AxisTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestConstructionNullName()
        {
            var axis = new Axis(null, AxisIdentifier.A);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructionEmptyName1()
        {
            var axis = new Axis(string.Empty, AxisIdentifier.A);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructionEmptyName2()
        {
            var axis = new Axis("    ", AxisIdentifier.A);
        }

        [TestMethod]
        public void TestConstructionLinear()
        {
            var name = "AxisName";
            var type = AxisType.Linear;

            var axis = new Axis(name, AxisIdentifier.X);

            Assert.AreEqual(name, axis.Name);
            Assert.AreEqual(type, axis.AxisType);
        }

        [TestMethod]
        public void TestConstructionRotational()
        {
            var name = "AxisName";
            var type = AxisType.Rotational;

            var axis = new Axis(name, AxisIdentifier.A);

            Assert.AreEqual(name, axis.Name);
            Assert.AreEqual(type, axis.AxisType);
        }
    }
}
