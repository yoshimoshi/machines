﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Yoshimoshi.Machines.Unit.Test
{
    [TestClass]
    public class AxisCollectionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestConstructionNullCollection()
        {
            var collection = new AxisCollection(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructionNullMember()
        {
            var collection = new AxisCollection(new Axis[]
                {
                    new Axis("Foo", AxisIdentifier.X),
                    null
                });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructionDuplicatename()
        {
            var collection = new AxisCollection(new Axis[]
                {
                    new Axis("Foo", AxisIdentifier.X),
                    new Axis("Bar", AxisIdentifier.Y),
                    new Axis("Foo", AxisIdentifier.X)
                });
        }

        [TestMethod]
        public void TestConstruction()
        {
            var definition = new Axis[]
                {
                    new Axis("A", AxisIdentifier.A),
                    new Axis("B", AxisIdentifier.B),
                    new Axis("C", AxisIdentifier.C),
                    new Axis("D", AxisIdentifier.D),
                    new Axis("E", AxisIdentifier.E),
                    new Axis("F", AxisIdentifier.F)
                };

            // check count
            var collection = new AxisCollection(definition);
            Assert.AreEqual(definition.Length, collection.Count);

            var i = 0;
            // make sure none are null
            for (i = 0; i < definition.Length; i++)
            {
                Assert.IsNotNull(collection[i]);
            }

            // verify we can iterate and that the collection items are correct
            i = 0;
            foreach (var a in collection)
            {
                Assert.AreEqual(definition[i++], a);
            }
        }
    }
}
