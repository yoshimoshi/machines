﻿using System;
using System.IO;

namespace Yoshimoshi.Machines.GCode
{
    /*
    public enum GCodes
    {
        /// <summary>
        /// Linear Move
        /// </summary>
        G0,
        /// <summary>
        /// Fast Linear Move
        /// </summary>
        G1,
        /// <summary>
        /// Arc move clockwise
        /// </summary>
        G2,
        /// <summary>
        /// Arc move counterclockwise
        /// </summary>
        G3,
        /// <summary>
        /// Dwell
        /// </summary>
        G4,
        /// <summary>
        /// Imperial Units
        /// </summary>
        G20,
        /// <summary>
        /// Metric Units
        /// </summary>
        G21,
        /// <summary>
        /// Move to Origin
        /// </summary>
        G28,
        /// <summary>
        /// Use Absolute Positioning
        /// </summary>
        G90,
        /// <summary>
        /// Use Relative Positioning
        /// </summary>
        G91,
        /// <summary>
        /// Set Positiion
        /// </summary>
        G92
    }

    public enum MCodes
    {
        /// <summary>
        /// Air on
        /// </summary>
        M7,
        /// <summary>
        /// Coolant On
        /// </summary>
        M8,
        /// <summary>
        /// Coolant Off
        /// </summary>
        M9
    }

    public enum Commands
    {
        /// <summary>
        /// Surface transform
        /// </summary>
        TRACYL,
        /// <summary>
        /// Deselect transform
        /// </summary>
        TRAFOOF,
        /// <summary>
        /// Select Master Spindle
        /// </summary>
        SETMS

    }
    */
    public class NCReader
    {
        private TextReader m_reader;
        private Stream m_stream;

        public NCReader(string program)
        {
            if (string.IsNullOrEmpty(program))
            {
                throw new ArgumentException();
            }

            m_stream = new MemoryStream();
            var writer = new StreamWriter(m_stream);
            writer.Write(program);
            writer.Flush();
            m_stream.Position = 0;
        }

        private NCReader(Stream source)
        {
            if (!source.CanRead) throw new Exception("Unreadable stream");
            m_stream = source;
        }

        private TextReader Reader
        {
            get
            {
                if (m_reader == null)
                {
                    m_reader = new StreamReader(m_stream) as TextReader;
                }

                return m_reader;
            }
        }

        public static NCReader Open(Stream stream)
        {
            return new NCReader(stream);            
        }

        public void MoveToStart()
        {
            m_stream.Seek(0, SeekOrigin.Begin);
        }

        public Block NextBlock()
        {
            var text = Reader.ReadLine();
            if (text == null)
            {
                // end of file
                return null;
            }

            return new Block(text);
        }

        public Block Peek()
        {
            var position = m_stream.Position;
            var block = NextBlock();
            m_stream.Position = position;
            return block;
        }

        public bool HasLineNumbers
        {
            get
            {
                // TODO:
                return false;
            }
        }

        public void AddLineNumbersAndSave(Stream outputStream, int startLineNumber = 100, int lineNumberInterval = 10, int lineDigits = 8)
        {
            if (startLineNumber < 0) throw new ArgumentOutOfRangeException(nameof(startLineNumber));
            if (lineNumberInterval <= 0) throw new ArgumentOutOfRangeException(nameof(lineNumberInterval));
            // <= 0 means we just output with no padding.  More that 12 is ridiculous, so ignore it.
            if (lineDigits > 12) throw new ArgumentOutOfRangeException(nameof(lineDigits));

            var format = (lineDigits > 1) ? new string('0', lineDigits) : "0";
            format = $"N{{0:{format}}} {{1}}";

            // start of the source
            m_stream.Seek(0, SeekOrigin.Begin);

            // start of destination
            outputStream.Seek(0, SeekOrigin.Begin);

            // walk each line in the source
            using (var reader = new StreamReader(m_stream) as TextReader)
            using (var writer = new StreamWriter(outputStream) as TextWriter)
            {
                var currentLineNumber = startLineNumber;
                var line = reader.ReadLine();

                while (line != null)
                {
                    // work with a clean line only
                    line = line.Trim();

                    if (CanAddLineNumber(line))
                    {
                        // add a line number
                        if (line[0] == 'N')
                        {
                            // already has a number, we need to rewrite it
                            writer.WriteLine(string.Format(format, currentLineNumber, GetLineWithoutLineNumber(line)));
                        }
                        else
                        {
                            writer.WriteLine(string.Format(format, currentLineNumber, line));
                        }

                        // increment the line number
                        currentLineNumber += lineNumberInterval;
                    }
                    else
                    {
                        // it can't have a line number (comment, already numbered, etc) so just copy it
                        writer.WriteLine(line);
                    }
                    

                    line = reader.ReadLine();
                }
                // close the output stream
            }
        }

        private string GetLineWithoutLineNumber(string line)
        {
            if (line[0] != 'N') return line;

            var index = line.IndexOf(' ');
            if (index < 0) return string.Empty; // it's just an empty line (well a line with just a line number)

            return line.Substring(index + 1);
        }

        private bool CanAddLineNumber(string line)
        {
            if (string.IsNullOrWhiteSpace(line) || line.Length == 0) return false;

            switch (line[0])
            {
                case '%': // nc delimiter
                case ';': // comment
                    return false;
                case 'O': // potentially program name on a Haas
                    if (line == "Off") return true;
                    return false;
                default:
                    return true;
            }
        }
    }
}
