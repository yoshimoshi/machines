﻿namespace Yoshimoshi.Machines.GCode
{
    public class ToolCommand : Command
    {
        public int ToolNumber { get; private set; }

        internal ToolCommand(int toolNumber)
        {
            ToolNumber = toolNumber;
        }
    }
}
