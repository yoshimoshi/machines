﻿namespace Yoshimoshi.Machines.GCode
{
    /// <summary>
    /// Dwell
    /// </summary>
    public class G04 : Command
    {
        /// <summary>
        /// Dwell (pause) time, in milliseconds
        /// </summary>
        public int DwellTime { get; private set; }

        internal G04(int dwellTime)
        {
            DwellTime = dwellTime;
        }
    }
}
