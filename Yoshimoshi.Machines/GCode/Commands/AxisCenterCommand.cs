﻿namespace Yoshimoshi.Machines.GCode
{
    public class AxisCenterCommand : Command
    {
        public AxisIdentifier AxisIdentifier { get; private set; }
        public double Position { get; private set; }

        internal AxisCenterCommand(AxisIdentifier axis, double position)
        {
            AxisIdentifier = axis;
            Position = position;
        }
    }
}
