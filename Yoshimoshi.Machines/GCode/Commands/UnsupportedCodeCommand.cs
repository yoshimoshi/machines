﻿namespace Yoshimoshi.Machines.GCode
{
    public class UnsupportedCodeCommand : Command
    {
        public string Text { get; private set; }

        internal UnsupportedCodeCommand(string text)
        {
            Text = text;
        }
    }

}
