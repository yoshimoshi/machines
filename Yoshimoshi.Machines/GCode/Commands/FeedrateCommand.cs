﻿namespace Yoshimoshi.Machines.GCode
{
    public class FeedrateCommand : Command
    {
        public double Rate { get; private set; }

        internal FeedrateCommand(double rate)
        {
            Rate = rate;
        }
    }
}
