﻿namespace Yoshimoshi.Machines.GCode
{
    public class AxisMoveCommand : Command
    {
        public AxisIdentifier AxisIdentifier { get; private set; }
        public double Position { get; private set; }

        internal AxisMoveCommand(AxisIdentifier axis, double position)
        {
            AxisIdentifier = axis;
            Position = position;
        }
    }
}
