﻿namespace Yoshimoshi.Machines.GCode
{
    public class SpindleSpeedCommand : Command
    {
        public double Speed { get; private set; }

        internal SpindleSpeedCommand(double speed)
        {
            Speed = speed;
        }
    }
}
