﻿namespace Yoshimoshi.Machines.GCode
{
    public abstract class Command
    {
        internal static Command Parse(string text, string nextCommand, out bool parameterConsumed)
        {
            parameterConsumed = false;

            double d;
            int i;

            switch (text[0])
            {
                case 'A':
                    return new AxisMoveCommand(AxisIdentifier.A, double.Parse(text.Substring(1)));
                case 'B':
                    return new AxisMoveCommand(AxisIdentifier.B, double.Parse(text.Substring(1)));
                case 'C':
                    // e.g. C=DC(-35228.191)
                    if (text[1] == '=')
                    {
                        // TODO: figure this out
                        return new AxisMoveCommand(AxisIdentifier.C, 0.0);
                    }
                    else
                    {
                        if (double.TryParse(text.Substring(1), out d))
                        {
                            return new AxisMoveCommand(AxisIdentifier.C, d);
                        }
                        else
                        {
                            return new UnsupportedCodeCommand(text);
                        }
                    }
                case 'F':
                    return new FeedrateCommand(double.Parse(text.Substring(1)));
                case 'G':
                    var gcode = GetCodeNumber(text);
                    if (gcode == null)
                    {
                        return new UnsupportedCodeCommand(text);
                    }
                    switch (gcode.Value)
                    {
                        case 0:
                            return new G00();
                        case 1:
                            return new G01();
                        case 4:
                            parameterConsumed = true;

                            if (nextCommand == null)
                            {
                                // no time provided so it's a "dwell for no time"
                                // TODO: is this even valid GCode?
                                return new G04(0);
                            }

                            // dwell period often starts with 'P' but ion some cases may be 'X' or 'U'
                            if (double.TryParse(nextCommand.Substring(1), out double dwell))
                            {

                                switch (nextCommand[0])
                                {
                                    case 'P':
                                        // this is in milliseconds, unless it has a period in it, in which case it's seconds
                                        if (nextCommand.Contains("."))
                                        {
                                            return new G04((int)(dwell * 1000));
                                        }
                                        return new G04((int)dwell);
                                    case 'X':
                                    case 'U':
                                        // this is in seconds
                                        return new G04((int)(dwell * 1000));
                                }
                            }
                            return new UnsupportedCodeCommand(text);
                        case 90:
                            return new G90();
                        case 91:
                            return new G91();
                        default:
                            return new UnsupportedCodeCommand(text);
                    }
                case 'I':
                    return new AxisCenterCommand(AxisIdentifier.X, double.Parse(text.Substring(1)));
                case 'J':
                    return new AxisCenterCommand(AxisIdentifier.X, double.Parse(text.Substring(1)));
                case 'M':
                    var mcode = GetCodeNumber(text);
                    if (mcode == null)
                    {
                        return new UnsupportedCodeCommand(text);
                    }
                    switch (mcode.Value)
                    {
                        case 6:
                            return new M06();
                        default:
                            return new UnsupportedCodeCommand(text);
                    }
                case 'S':
                    if (double.TryParse(text.Substring(1), out d))
                    {
                        return new SpindleSpeedCommand(d);
                    }
                    else
                    {
                        return new UnsupportedCodeCommand(text);
                    }
                case 'T':
                    if (int.TryParse(text.Substring(1), out i))
                    {
                        return new ToolCommand(i);
                    }
                    else
                    {
                        return new UnsupportedCodeCommand(text);
                    }
                case 'X':
                    return new AxisMoveCommand(AxisIdentifier.X, double.Parse(text.Substring(1)));
                case 'Y':
                    return new AxisMoveCommand(AxisIdentifier.Y, double.Parse(text.Substring(1)));
                case 'Z':
                    return new AxisMoveCommand(AxisIdentifier.Z, double.Parse(text.Substring(1)));
                default:
                    return new UnsupportedCodeCommand(text);
            }
        }

        private static int? GetCodeNumber(string command)
        {
            int i;
            if (int.TryParse(command.Substring(1), out i))
            {
                return i;
            }
            return null;
        }
    }

}
