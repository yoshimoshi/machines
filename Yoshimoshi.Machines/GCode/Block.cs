﻿using System;
using System.Collections.Generic;

namespace Yoshimoshi.Machines.GCode
{
    public class Block
    {
        public string Text { get; }
        public string BlockNumber { get; set; }
        public List<Command> Commands { get; } = new List<Command>();

        internal Block(string text)
        {
            Text = text;
            Parse(text);
        }

        private void Parse(string text)
        {
            // is there anything to parse?
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            // nothing to parse for comments
            if (text[0] == ';')
            {
                return;
            }

            // look for trailing comments and strip them
            var index = text.IndexOf(';');
            if (index >= 0)
            {
                text = text.Substring(0, index);
            }

            // is there anything left to parse?
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            // break into command segments
            var segments = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (segments.Length == 0)
            {
                return;
            }

            var hasLineNumber = false;

            // look for a line number
            if (segments[0][0] == 'N')
            {
                BlockNumber = segments[0];
                hasLineNumber = true;
            }

            // parse the rest of the commands
            for (int i = hasLineNumber ? 1 : 0; i < segments.Length; i++)
            {
                // pass the "next" command as well, in case it's a parameter
                Commands.Add(
                    Command.Parse(
                        segments[i],
                        (i < segments.Length - 1) ? segments[i + 1] : null,
                        out bool paramConsumed));

                if (paramConsumed) i++;
            }
        }

        public TimeSpan GetEstimatedExecutionTime(int feedrate)
        {
            return TimeSpan.FromMilliseconds(100);
        }
    }

}
