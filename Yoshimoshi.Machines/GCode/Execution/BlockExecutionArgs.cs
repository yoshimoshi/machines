﻿using System;

namespace Yoshimoshi.Machines.GCode
{
    public class BlockExecutionArgs : EventArgs
    {
        internal BlockExecutionArgs(Block block)
        {
            Block = block;
        }

        public Block Block { get; private set; }
    }

}
