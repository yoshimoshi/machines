﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Yoshimoshi.Machines.GCode
{
    public abstract class Executor
    {
        public event EventHandler<AxisPositionChangedArgs> XPositionChanged;
        public event EventHandler<AxisPositionChangedArgs> YPositionChanged;
        public event EventHandler<AxisPositionChangedArgs> ZPositionChanged;
        public event EventHandler<AxisPositionChangedArgs> APositionChanged;
        public event EventHandler<AxisPositionChangedArgs> BPositionChanged;
        public event EventHandler<AxisPositionChangedArgs> CPositionChanged;
        public event EventHandler<BlockExecutionArgs> BlockExecutionStarted;
        public event EventHandler<BlockExecutionArgs> BlockExecutionFinished;
        public event EventHandler<ValueChangedArgs> FeedRateChanged;
        public event EventHandler<ValueChangedArgs> SpeedChanged;

        private NCReader m_gCodeFile;
        private Block m_nextBlock;
        private double m_feedRate;
        private double m_speed;
        private Position m_commandedPosition = new Position();

        public string Name { get; protected set; }
        public double RapidMotionRate { get; set; }

        public int CurrentToolNumber { get; private set; }


        public MachineState State { get; set; }
        public bool Finished { get; set; }
        public string LineNumber { get; set; }
        public string ProgramBlock { get; set; }
        public int PartCount { get; set; }
        public bool Running { get; set; }
        public SpindleState SpindleState { get; set; }
        public bool FeedHold { get; set; }
        public int SelectedTool { get; set; }
        public int ToolNumber { get; set; }
        public string Error { get; private set; }

        public MotionType CurrentMotionType { get; private set; }
        public PositionType CurrentPositionType { get; private set; }
        public Units Units { get; private set; }
        public TimeSpan TotalRunTime { get; private set; }

        public double FeedRate
        {
            get => m_feedRate;
            set
            {
                m_feedRate = value;
                FeedRateChanged?.Invoke(this, new ValueChangedArgs(FeedRate));
            }
        }

        public double SpindleSpeed
        {
            get => m_speed;
            set
            {
                m_speed = value;
                SpeedChanged?.Invoke(this, new ValueChangedArgs(SpindleSpeed));
            }
        }

        public Position CommandedPosition
        {
            get => m_commandedPosition;
            protected set
            {
                if (value.X != CommandedPosition.X) XPositionChanged?.Invoke(this, new AxisPositionChangedArgs(AxisIdentifier.X, value.X));
                if (value.Y != CommandedPosition.Y) YPositionChanged?.Invoke(this, new AxisPositionChangedArgs(AxisIdentifier.Y, value.Y));
                if (value.Z != CommandedPosition.Z) ZPositionChanged?.Invoke(this, new AxisPositionChangedArgs(AxisIdentifier.Z, value.Z));
                if (value.A != CommandedPosition.A) APositionChanged?.Invoke(this, new AxisPositionChangedArgs(AxisIdentifier.A, value.A));
                if (value.B != CommandedPosition.B) BPositionChanged?.Invoke(this, new AxisPositionChangedArgs(AxisIdentifier.B, value.B));
                if (value.C != CommandedPosition.C) CPositionChanged?.Invoke(this, new AxisPositionChangedArgs(AxisIdentifier.C, value.C));
                m_commandedPosition = value;
            }
        }

        protected virtual async Task BeforeProgramRun()
        {
            await Task.Yield();
        }

        protected virtual async Task AfterProgramRun()
        {
            await Task.Yield();
        }

        protected virtual async Task BeforeExecuteBlock()
        {
            await Task.Yield();
        }

        protected virtual async Task AfterExecuteBlock()
        {
            await Task.Yield();
        }

        public async Task Run(NCReader program)
        {
            await Run(program, 0);
        }

        public async Task Run(NCReader program, double defaultFeedrate)
        {
            if (defaultFeedrate > 0)
            {
                FeedRate = defaultFeedrate;
            }

            await BeforeProgramRun();

            // load the program
            program.MoveToStart();

            var block = program.NextBlock();

            while (block != null)
            {
                var start = Environment.TickCount;
                await BeforeExecuteBlock();
                var time = await ExecuteBlock(block);
                await AfterExecuteBlock();
                var et = Environment.TickCount - start;

                TotalRunTime += time;
                TotalRunTime += TimeSpan.FromMilliseconds(et);

                block = program.NextBlock();
            }
        }

        protected virtual async Task<TimeSpan> ExecuteBlock(Block block)
        {
            var timeToExecute = TimeSpan.Zero;

            BlockExecutionStarted?.Invoke(this, new BlockExecutionArgs(block));

            foreach (var command in block.Commands)
            {
                timeToExecute += await ExecuteCommand(command);
            }

            BlockExecutionFinished?.Invoke(this, new BlockExecutionArgs(block));

            return timeToExecute;
        }

        protected virtual async Task<TimeSpan> ExecuteCommand(Command command)
        {
            if (command is G00) CurrentMotionType = MotionType.FastLinearMove;
            else if (command is G01) CurrentMotionType = MotionType.LinearMove;
            else if (command is G04) await Task.Delay((command as G04).DwellTime);
            else if (command is G20) Units = Units.Imperial;
            else if (command is G21) Units = Units.Metric;
            else if (command is G90) CurrentPositionType = PositionType.Absolute;
            else if (command is G91) CurrentPositionType = PositionType.Relative;
            else if (command is M06) CurrentToolNumber = SelectedTool;
            else if (command is ToolCommand) SelectedTool = (command as ToolCommand).ToolNumber; // TODO: add tool change time
            else if (command is FeedrateCommand) FeedRate = (command as FeedrateCommand).Rate;
            else if (command is SpindleSpeedCommand) SpindleSpeed = (command as SpindleSpeedCommand).Speed;
            else if (command is AxisMoveCommand)
            {
                // base class does nothing, but pretty much any implementation will need to handle this
            }

            // base class takes zero time to execute - implementations should  override and adjust
            return await Task.FromResult(TimeSpan.Zero);
        }

        public bool LoadFile(string fileName)
        {
            State = MachineState.Loading;
            string path = string.Format("C:\\NCPrograms\\{0}.ncc", fileName);
            if (!File.Exists(path))
            {
                return false;
            }

            try
            {
                var stream = File.Open(path, FileMode.Open);
                m_gCodeFile = NCReader.Open(stream);
                m_gCodeFile.MoveToStart();
            }
            catch (Exception ex)
            {
                Error = string.Format("Failed loading NC file -> '{0}' : {1}", fileName, ex.Message);
                return false;
            }
            return true;
        }

        public async Task ExecuteNextBlock()
        {
            if (State == MachineState.Running && !FeedHold)
            {
                if (m_nextBlock == null)
                {
                    State = MachineState.Done;
                    return;
                }

                await ExecuteBlock(m_nextBlock);
                m_nextBlock = m_gCodeFile.NextBlock();
                if (m_nextBlock == null)
                {
                    State = MachineState.Done;
                }
            }
        }

        public void CycleStart()
        {
            State = MachineState.Running;
        }

        public void CycleStop()
        {
            Running = false;
        }

    }
}
