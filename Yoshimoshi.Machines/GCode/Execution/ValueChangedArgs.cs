﻿using System;

namespace Yoshimoshi.Machines.GCode
{
    public class ValueChangedArgs : EventArgs
    {
        internal ValueChangedArgs(double value)
        {
            Value = value;
        }

        public double Value { get; private set; }
    }

}
