﻿using System;

namespace Yoshimoshi.Machines.GCode
{
    public class Position
    {
        public Position()
        {
        }

        public Position(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Position(Position position)
        {
            X = position.X;
            Y = position.X;
            Z = position.X;
            A = position.A;
            B = position.B;
            C = position.C;
            D = position.D;
            E = position.E;
            F = position.F;
        }

        public double LinearDistanceTo(Position other)
        {
            var dx = X - other.X;
            var dy = Y - other.Y;
            var dz = Z - other.Z;
            return Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2) + Math.Pow(dz, 2));
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }
        public double E { get; set; }
        public double F { get; set; }
    }

}
