﻿using System;

namespace Yoshimoshi.Machines.GCode
{
    public class AxisPositionChangedArgs : EventArgs
    {
        internal AxisPositionChangedArgs(AxisIdentifier axisIdentifier, double position)
        {
            AxisIdentifier = axisIdentifier;
            Position = position;
        }

        public AxisIdentifier AxisIdentifier { get; private set; }
        public double Position { get; private set; }
    }

}
