﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Yoshimoshi.Machines.GCode
{
    internal class CommandExecutionResult
    {
        public TimeSpan ExecutionTime { get; set; }
        public Position EndPosition { get; set; }
    }

    public class SimulationExecutor : Executor
    {
        public const double DefaultRapidMotionRate = 600; // in/sec
        public const double DefaultFeedrate = 100; // in/sec

        private DateTime m_outputStartTime;
        private DateTime m_outputLastWrite;
        private DateTime m_outputEndTime;
        private bool m_outputFirstWrite;
        private int m_outputIntervalSeconds = 15;
        private StreamWriter m_outputFile;
        private Position m_newPosition = null;
        private bool m_positionChanged = false;

        public double ExecutionSpeedFactor { get; set; } = -1.0d;

        public SimulationExecutor(string name = "Simulation")
        {
            RapidMotionRate = DefaultRapidMotionRate;
            Name = name;
            SpindleState = SpindleState.Stopped;
            SpindleSpeed = 0;
            State = MachineState.Idle;
            ToolNumber = 1;
        }

        protected override async Task BeforeProgramRun()
        {
            await base.BeforeProgramRun();

            m_outputFile = new System.IO.StreamWriter(@"C:\Temp\Output.csv");
            m_outputFile.WriteLine("Time,Block,X,Y,Z,B,C,FeedRate,SpindleSpeed");
            m_outputStartTime = DateTime.Now;
            m_outputLastWrite = m_outputStartTime;
            m_outputEndTime = m_outputStartTime;
            m_outputFirstWrite = true;
        }

        protected override async Task AfterProgramRun()
        {
            await base.AfterProgramRun();

            m_outputFile.Close();
        }

        protected override async Task BeforeExecuteBlock()
        {
            await base.BeforeExecuteBlock();
            m_newPosition = new Position(CommandedPosition);
        }

        protected override async Task AfterExecuteBlock()
        {
            await base.AfterExecuteBlock();

            if (m_newPosition != null && m_positionChanged)
            {
                CommandedPosition = m_newPosition;
            }
        }

        protected override async Task<TimeSpan> ExecuteCommand(Command command)
        {
            if (command is AxisMoveCommand)
            {
                var result = await ExecuteMovement(command as AxisMoveCommand, m_newPosition);
                m_newPosition = result.EndPosition;
                m_positionChanged = true;
                return result.ExecutionTime;
            }
            else
            {
                return await base.ExecuteCommand(command);
            }
        }

        private async Task<CommandExecutionResult> ExecuteMovement(AxisMoveCommand command, Position startPosition)
        {
            // default to zero execution time
            var executeTime = TimeSpan.Zero;

            var newPosition = new Position(startPosition);

            // if units == imperial, feed is in/min or in/rev, metric is mm/min or mm/rev
            // NOTE: we assume instantaneous acceleration here
            var amc = command as AxisMoveCommand;
            switch (amc.AxisIdentifier)
            {
                case AxisIdentifier.X:
                    if (CurrentPositionType == PositionType.Absolute)
                    {
                        newPosition.X = amc.Position;
                    }
                    else
                    {
                        newPosition.X += amc.Position;
                    }
                    break;
                case AxisIdentifier.Y:
                    if (CurrentPositionType == PositionType.Absolute)
                    {
                        newPosition.Y = amc.Position;
                    }
                    else
                    {
                        newPosition.Y += amc.Position;
                    }
                    break;
                case AxisIdentifier.Z:
                    if (CurrentPositionType == PositionType.Absolute)
                    {
                        newPosition.Z = amc.Position;
                    }
                    else
                    {
                        newPosition.Z += amc.Position;
                    }
                    break;
                case AxisIdentifier.A:
                    if (CurrentPositionType == PositionType.Absolute)
                    {
                        newPosition.A = amc.Position;
                    }
                    else
                    {
                        newPosition.A += amc.Position;
                    }
                    break;
                case AxisIdentifier.B:
                    if (CurrentPositionType == PositionType.Absolute)
                    {
                        newPosition.B = amc.Position;
                    }
                    else
                    {
                        newPosition.B += amc.Position;
                    }
                    break;
                case AxisIdentifier.C:
                    if (CurrentPositionType == PositionType.Absolute)
                    {
                        newPosition.C = amc.Position;
                    }
                    else
                    {
                        newPosition.C += amc.Position;
                    }
                    break;
            }

            var distance = CommandedPosition.LinearDistanceTo(newPosition);

            // feedrate is always expressed in minutes
            switch (this.CurrentMotionType)
            {
                case MotionType.FastLinearMove:
                    if (RapidMotionRate > 0)
                    {
                        executeTime = TimeSpan.FromMinutes(distance / RapidMotionRate);
                    }
                    break;
                case MotionType.LinearMove:
                // TODO: improve this for rotational moves
                case MotionType.ClockwiseRotation:
                case MotionType.CounterclockwiseRotation:
                    if (FeedRate > 0)
                    {
                        executeTime = TimeSpan.FromMinutes(distance / FeedRate);
                    }
                    break;
            }

            m_outputEndTime += executeTime;

            if ((m_outputEndTime - m_outputLastWrite).TotalSeconds > m_outputIntervalSeconds ||
                m_outputFirstWrite)
            {
                m_outputFile.WriteLine($"{m_outputEndTime.ToString()},{command.ToString()},{newPosition.X},{newPosition.Y},{newPosition.Z},{newPosition.B},{newPosition.C},{FeedRate},{SpindleSpeed}");
                Debug.WriteLine($"{m_outputEndTime.ToString()},{command.ToString()},{newPosition.X},{newPosition.Y},{newPosition.Z},{newPosition.B},{newPosition.C},{SpindleSpeed},{FeedRate}, - Move takes {executeTime.TotalSeconds} seconds");
                m_outputLastWrite = m_outputEndTime;
                m_outputFirstWrite = false;
            }

            if (ExecutionSpeedFactor > 0)
            {
                await Task.Delay((int)(executeTime.TotalMilliseconds / ExecutionSpeedFactor));
            }

            return new CommandExecutionResult()
            {
                ExecutionTime = executeTime,
                EndPosition = newPosition
            };

        }
    }
}
