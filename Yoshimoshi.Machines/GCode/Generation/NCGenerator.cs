﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yoshimoshi.Machines.GCode
{
    public class NCGenerator
    {
        private List<string> m_commands = new List<string>();
        private PositionType? m_positionType;
        private Dictionary<AxisIdentifier, double> m_previousPositions = new Dictionary<AxisIdentifier, double>();

        public NCGenerator()
        {
        }

        public string LastLine
        {
            get
            {
                if (m_commands.Count == 0) return string.Empty;
                return m_commands[m_commands.Count - 1];
            }
        }

        public PositionType? CurrentPositionType
        {
            get => m_positionType;
            set
            {
                if (value.HasValue && value != m_positionType)
                {
                    switch (value.Value)
                    {
                        case PositionType.Absolute:
                            m_commands.Add("G90 ;ABSOLUTE POSITIONING");
                            break;
                        case PositionType.Relative:
                            m_commands.Add("G91 ;RELATIVE POSITIONING");
                            break;
                    }

                    m_positionType = value;
                }
            }
        }

        public void Start()
        {
            m_commands.Clear();
            m_previousPositions.Clear();
            m_positionType = null;
        }

        public void AddFeedrate(int feedrate)
        {
            m_commands.Add($"F{feedrate} ; CHANGE FEEDRATE");
        }

        public void AddDwell(int dwellTime)
        {
            m_commands.Add($"G04 P{dwellTime} ; DWELL");
        }

        public void AddMove(MotionType motionType, Movement movement)
        {
            if (movement.PositionType != CurrentPositionType)
            {
                CurrentPositionType = movement.PositionType;
            }

            var line = new StringBuilder();

            switch (motionType)
            {
                case MotionType.FastLinearMove:
                    line.Append("G00");
                    break;
                case MotionType.LinearMove:
                    line.Append("G01");
                    break;
                case MotionType.ClockwiseRotation:
                    line.Append("G02");
                    break;
                case MotionType.CounterclockwiseRotation:
                    line.Append("G03");
                    break;
            }

            var isActualMove = false;

            if (CurrentPositionType == PositionType.Absolute)
            {
                foreach (var m in movement.Moves)
                {
                    if (m_previousPositions.TryGetValue(m.Axis.AxisIdentifier, out double previousPosition))
                    {
                        // there is a previous known position, has it changed?
                        if (m.NewPosition != previousPosition)
                        {
                            line.Append($" {m.Axis.AxisIdentifier.ToString()}{m.NewPosition}");

                            // update the known position
                            m_previousPositions[m.Axis.AxisIdentifier] = m.NewPosition;
                            isActualMove = true;
                        }
                    }
                    else
                    {
                        // we have no previous position
                        line.Append($" {m.Axis.AxisIdentifier.ToString()}{m.NewPosition}");

                        // record the known position
                        m_previousPositions[m.Axis.AxisIdentifier] = m.NewPosition;
                        isActualMove = true;
                    }
                }

                // only append *if* some axis has actually moved
                if (isActualMove)
                {
                    m_commands.Add(line.ToString());
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public string GetNCCode(int? programNumber = null, string programDescription = null)
        {
            var sb = new StringBuilder();
            sb.AppendLine("%"); // start of file

            var lineNumber = 10;

            if (programNumber.HasValue)
            {
                if (string.IsNullOrWhiteSpace(programDescription))
                {
                    sb.AppendLine($"O{programNumber.Value}");
                }
                else
                {
                    sb.AppendLine($"O{programNumber.Value:D6} ({programDescription})");
                }
            }
            else if(!string.IsNullOrWhiteSpace(programDescription))
            {
                sb.AppendLine($"({programDescription})");
            }

            foreach (var command in m_commands)
            {
                sb.AppendLine($"N{lineNumber:D4} {command}");

                // TODO: support adjusting line numbering

                lineNumber += 10;
            }

            // TODO support adjusting end of program
            sb.AppendLine($"N{lineNumber:D4} M30 ;END OF PROGRAM"); // end of program
            sb.Append("%"); // end of file
            return sb.ToString();
        }
    }
}
