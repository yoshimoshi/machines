﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yoshimoshi.Machines
{
    public class Axis : IAxis
    {
        public AxisType AxisType { get; }
        public AxisIdentifier AxisIdentifier { get; set; }
        public string Name { get; }

        public double CurrentPosition { get; internal set; }
        public double? MinPosition { get; }
        public double? MaxPosition { get; }

        public Axis(string name, AxisIdentifier identifier, double? minPosition = null, double? maxPosition = null, double? currentPosition = null)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Axis name must not be empty");

            Name = name;
            AxisIdentifier = identifier;
            switch (AxisIdentifier)
            {
                case AxisIdentifier.X:
                case AxisIdentifier.Y:
                case AxisIdentifier.Z:
                    AxisType = AxisType.Linear;
                    break;
                default:
                    AxisType = AxisType.Rotational;
                    break;
            }
            MinPosition = minPosition;
            MaxPosition = maxPosition;

            if (currentPosition.HasValue) CurrentPosition = currentPosition.Value;
        }

    }
}
