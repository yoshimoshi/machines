﻿using System.Collections.Generic;
using System.Linq;

namespace Yoshimoshi.Machines
{
    public class Movement
    {
        public List<AxisMove> Moves { get; } = new List<AxisMove>();
        public int? Feedrate { get; set; }

        public PositionType PositionType { get; }

        public Movement(PositionType moveType)
        {
            PositionType = moveType;
        }

        public Movement(PositionType moveType, params AxisMove[] axisMoves)
            : this(moveType, (IEnumerable<AxisMove>)axisMoves)
        {
        }

        public Movement(PositionType moveType, int feedrate, params AxisMove[] axisMoves)
            : this(moveType, (IEnumerable<AxisMove>)axisMoves)
        {
            Feedrate = feedrate;
        }

        public Movement(PositionType moveType, int feedrate, IEnumerable<AxisMove> axisMoves)
            : this(moveType, axisMoves)
        {
            Feedrate = feedrate;
        }

        public Movement(PositionType moveType, IEnumerable<AxisMove> axisMoves)
        {
            PositionType = moveType;
            Moves = axisMoves.ToList();
        }

    }
}
