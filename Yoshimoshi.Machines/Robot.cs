﻿using System;
using System.Collections.Generic;

namespace Yoshimoshi.Machines
{
    public abstract class Robot : Machine
    {
        // this is a simple naming alias
        public AxisCollection Joints { get => Axes; }

        public Robot(IEnumerable<Axis> axes)
            : base(axes)
        {
        }

        /// <summary>
        /// Creates a Machine with the given number of rotational axes.
        /// </summary>
        /// <param name="degreesOfFreedom"></param>
        public Robot(int degreesOfFreedom)
        {
            if (degreesOfFreedom <= 0) throw new ArgumentException("Degrees of freedom must be greater than zero");

            var axes = new Axis[degreesOfFreedom];

            // start with Axis A and increment
            var id = AxisIdentifier.A;
            for (int i = 0; i < degreesOfFreedom; i++)
            {
                axes[i] = new Axis($"J{i}", id++);
            }
        }
    }

}
