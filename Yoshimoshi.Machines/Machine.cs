﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yoshimoshi.Machines
{
    public abstract class Machine
    {
        public abstract bool IsMoving { get; protected set;  }
        public abstract Task<bool> Move(Movement movement);

        public AxisCollection Axes { get; protected set; }

        protected Machine()
        {
        }

        protected Machine(params Axis[] axes)
            : this((IEnumerable<Axis>) axes)
        {
        }

        public Machine(IEnumerable<Axis> axes)
        {
            Axes = new AxisCollection(axes);
        }

        protected void AfterMoveCompleted(Movement movement)
        {
            foreach (var m in movement.Moves)
            {
                var axis = Axes.First(a => a == m.Axis);
                if (movement.PositionType == PositionType.Absolute)
                {
                    axis.CurrentPosition = m.NewPosition;
                }
                else
                {
                    axis.CurrentPosition += m.NewPosition;
                }
            }
        }

    }
}
