﻿namespace Yoshimoshi.Machines
{
    public enum PositionType
    {
        Absolute,
        Relative
    }
}
