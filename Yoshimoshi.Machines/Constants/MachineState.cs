﻿namespace Yoshimoshi.Machines
{
    public enum MachineState
    {
        Idle = 1,
        Loading = 2,
        Running = 3,
        Done = 4,
   }
}
