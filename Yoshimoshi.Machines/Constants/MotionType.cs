﻿namespace Yoshimoshi.Machines
{
    public enum MotionType
    {
        LinearMove,
        FastLinearMove,
        ClockwiseRotation,
        CounterclockwiseRotation
    }
}
