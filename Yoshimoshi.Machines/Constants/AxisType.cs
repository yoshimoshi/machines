﻿namespace Yoshimoshi.Machines
{
    public enum AxisType
    {
        Linear,
        Rotational
    }

    public enum AxisIdentifier
    {
        A,
        B,
        C,
        D,
        E,
        F,
        X,
        Y,
        Z,
        Unidentified = 100
    }

}
