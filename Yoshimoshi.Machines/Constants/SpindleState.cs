﻿namespace Yoshimoshi.Machines
{
    public enum SpindleState
    {
        Stopped,
        Clockwise,
        CounterClockwise,
    }
}
