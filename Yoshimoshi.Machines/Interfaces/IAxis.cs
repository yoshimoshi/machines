﻿namespace Yoshimoshi.Machines
{
    public interface IAxis
    {
        AxisType AxisType { get; }
        string Name { get; }
        double CurrentPosition { get; }
    }
}
