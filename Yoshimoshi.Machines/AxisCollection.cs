﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Yoshimoshi.Machines
{
    public class AxisCollection : IEnumerable<Axis>
    {
        private readonly Axis[] m_axes;

        public AxisCollection(IEnumerable<Axis> axes)
        {
            if (axes == null) throw new ArgumentNullException();
            var count = axes.Count();
            if (count <= 0) throw new ArgumentException("Axis count must be greater than zero");

            m_axes = new Axis[count];

            var i = 0;
            foreach(var axis in axes)
            {
                if (axis == null)
                {
                    if (axis == null) throw new ArgumentNullException($"Axis {i} cannot be null");
                }

                // we cannot have duplicate axis names in the collection
                var existing = m_axes.FirstOrDefault(a => a != null && a.Name == axis.Name);

                if(existing != null)
                {
                    throw new ArgumentException($"Duplicate axis names not allowed ({axis.Name})");
                }

                m_axes[i++] = axis;
            }
        }

        public int Count
        {
            get => m_axes.Length;
        }

        public IEnumerator<Axis> GetEnumerator()
        {
            return ((IEnumerable<Axis>)m_axes).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Axis this[int index]
        {
            get
            {
                return m_axes[index];
            }
        }
    }

}
