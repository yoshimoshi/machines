﻿namespace Yoshimoshi.Machines
{
    public class AxisMove
    {
        public Axis Axis { get; }
        public double NewPosition { get; }

        public AxisMove(Axis axis, double newPosition)
        {
            Axis = axis;

            if (axis.MinPosition.HasValue)
            {
                if (newPosition < axis.MinPosition.Value)
                {
                    // TODO: add option to set an error if this happens
                    newPosition = axis.MinPosition.Value;
                }
            }

            if (axis.MaxPosition.HasValue)
            {
                if (newPosition > axis.MaxPosition.Value)
                {
                    // TODO: add option to set an error if this happens
                    newPosition = axis.MaxPosition.Value;
                }
            }

            this.NewPosition = newPosition;
        }
    }
}
