﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Yoshimoshi.Machines.GCode;

namespace Yoshimoshi.Machines.LittleArm
{
    /// <summary>
    /// Provides execution logic using the default LittleArm Arduino controller code
    /// </summary>
    public class LittleArmExecutor : Executor
    {
        private LittleArm.Big m_robot;
        private Movement m_movement = null;

        public LittleArmExecutor(LittleArm.Big robot)
        {
            m_robot = robot;
        }

        protected override async Task BeforeExecuteBlock()
        {
            await base.BeforeExecuteBlock();
            m_movement = null;
        }

        protected override async Task AfterExecuteBlock()
        {
            await base.AfterExecuteBlock();

            // physically move the robot
            if (m_movement != null)
            {
                if (await m_robot.Move(m_movement))
                {
                    // TODO: update for all axes
                    var newPos = new Position()
                    {
                        A = m_robot.Axes[0].CurrentPosition,
                        B = m_robot.Axes[1].CurrentPosition,
                        C = m_robot.Axes[2].CurrentPosition,
                        D = m_robot.Axes[3].CurrentPosition,
                        E = m_robot.Axes[4].CurrentPosition,
                        F = m_robot.Axes[5].CurrentPosition,
                    };
                    CommandedPosition = newPos;
                }
            }
        }

        protected override async Task<TimeSpan> ExecuteCommand(Command command)
        {
            // multiple axis moves may be in a single block
            // we'll just calculate all of the moves we need to send to the robot between before and after, 
            // then send during the "after"
            if (command is AxisMoveCommand)
            {
                var amc = command as AxisMoveCommand;

                if (m_movement == null)
                {
                    m_movement = new Movement(CurrentPositionType);
                }

                var targetAxis = m_robot.Axes.FirstOrDefault(a => a.AxisIdentifier == amc.AxisIdentifier);
                if (targetAxis == null)
                {
                    throw new Exception($"Unknown axis {amc.AxisIdentifier}");
                }

                var move = new AxisMove(targetAxis, amc.Position);
                m_movement.Moves.Add(move);

                // we took no time to determine the move, actual move time will get added by the AfterExecuteBlock
                return await Task.FromResult(TimeSpan.Zero);
            }
            else if (command is FeedrateCommand)
            {
                base.FeedRate = (command as FeedrateCommand).Rate;
                return await Task.FromResult(TimeSpan.Zero);
            }
            else
            {
                return await base.ExecuteCommand(command);
            }
        }
    }
}
