﻿namespace Yoshimoshi.Machines.LittleArm
{
    /// <summary>
    /// Represents the 6-axis LittleArm Big Robot
    /// </summary>
    public class Big : RobotBase
    {
        public Big(string serialPort)
            : base(serialPort)
        {
        }
    }
}
