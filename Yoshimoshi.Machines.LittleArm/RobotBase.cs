﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yoshimoshi.Machines.LittleArm
{
    public abstract class RobotBase : Robot, IDisposable
    {
        public const int DefaultBaudRate = 9600;
        public const int DefaultDefinedJoints = 6;
        public const int DefaultMovementSpeedDelay = 0;

        protected const int JointIndexBase = 0;
        protected const int JointIndexShoulder = 1;
        protected const int JointIndexElbow = 2;
        protected const int JointIndexWristRotate = 3;
        protected const int JointIndexWristFlex = 4;
        protected const int JointIndexGripper = 5;

        private SerialPort m_port;
        private int[] m_knownPositions;
        private int m_delay;
        private StringBuilder m_commandBuilder = new StringBuilder();
        private int m_feedrate = 100;
        private const int DelayMsPerPercentagePoint = 2;

        public override bool IsMoving { get; protected set; }

        public RobotBase(string portName, int degreesOfFreedom = DefaultDefinedJoints)
            : base(degreesOfFreedom)
        {
            // the LittleArm hardware supports up to 9 servos, but the default firmware is set up for 6
            if (degreesOfFreedom > 9) throw new ArgumentException("The LittleArm controller only supports up to 9 servos");

            m_port = new SerialPort(portName, DefaultBaudRate);
            m_knownPositions = new int[degreesOfFreedom];
            m_delay = DefaultMovementSpeedDelay;

            var axes = new Axis[degreesOfFreedom];

            var id = AxisIdentifier.A;

            for (int i = 0; i < degreesOfFreedom; i++)
            {
                axes[i] = new Axis($"J{i}", id++, 0, 180);
            }

            Axes = new AxisCollection(axes);
        }

        public void Dispose()
        {
            m_port.Dispose();
        }

        public async override Task<bool> Move(Movement movement)
        {
            if (IsMoving)
            {
                Debug.WriteLine($"Move in progress");
                return false;
            }

            IsMoving = true;

            try
            {
                if (!m_port.IsOpen)
                {
                    m_port.ReadTimeout = 50;
                    m_port.WriteTimeout = 50;
                    m_port.Open();
                }

                var command = GenerateMovementCommand(movement);

                if (command == null)
                {
                    // nothing to do
                    return true;
                }

                var moveSuccess = await Task.Factory.StartNew(() =>
                {
                    Debug.Write($"TX:{command}");
                    try
                    {
                        m_port.Write(command);

                        // the arduno controller will send back a "d" when the command is done executing
                        var response = (char)m_port.ReadChar();
                        Debug.WriteLine($"RX:{response}");

                        return true;
                    }
                    catch (TimeoutException)
                    {
                        Debug.WriteLine($"Port locked.  Resetting...");
                        Task.Delay(500);
                        m_port.Close();
                        Task.Delay(500);
                        m_port.Open();
                        return false;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine($"Write error: { ex.Message}");
                        return false;
                    }
                });

                Debug.WriteLine($"POST RX");

                if (moveSuccess)
                {
                    var maxChangeDegrees = 0d;

                    // Copy the movement values to the known positions
                    for (int i = 0; i < movement.Moves.Count; i++)
                    {
                        var delta = Math.Abs(m_knownPositions[i] - (int)movement.Moves[i].NewPosition);
                        if (delta > maxChangeDegrees) maxChangeDegrees = delta;

                        m_knownPositions[i] = (int)movement.Moves[i].NewPosition;
                    }

                    // the LittleArm controller does not wait for the move to physically finish before responding
                    // we must wait for it to finish, and that depends on distance and rate
                    var delayms = maxChangeDegrees / RotationRate * 1000d;
                    await Task.Delay((int)(delayms));
                }

                return moveSuccess;
            }
            finally
            {
                AfterMoveCompleted(movement);
                IsMoving = false;
            }
        }

        /// <summary>
        /// Current servo rotation rate in degrees per second
        /// </summary>
        private double RotationRate
        {
            // a fairly typical rate for a servo is ~1s for 180 degree sweep
            // TODO: account for "speed"
            get => 180;
        }

        public int Feedrate
        {
            get => m_feedrate;
            set
            {
                if (Feedrate == value) return;
                if ((value > 100) || (value < 1)) return;

                // this is a "percentage" value.  The way the driver (arduino code) itself works is by introducing delays to movements to slow itself.  
                // 100% rate means zero delay introduced.  We'll arbitraily call each percentage point being [X]ms delay added.
                m_delay = (int)((double)(100 - value) * 0.5);
                m_feedrate = value;
            }
        }

        protected virtual string GenerateMovementCommand(Movement movement)
        {
            var isActualMove = false;

            if (movement.Feedrate.HasValue)
            {
                Feedrate = movement.Feedrate.Value;
            }

            m_commandBuilder.Clear();

            for (int i = 0; i < m_knownPositions.Length; i++)
            {
                var request = movement.Moves.FirstOrDefault(m => m.Axis.Name == $"J{i}");
                if (request == null)
                {
                    // joint was not asked to move, just copy the known position
                    m_commandBuilder.Append(m_knownPositions[i].ToString());
                }
                else
                {
                    // is this actually a new position for the joint?
                    if (request.NewPosition != m_knownPositions[i])
                    {
                        m_commandBuilder.Append(((int)request.NewPosition).ToString());
                        isActualMove = true;
                    }
                    else
                    {
                        // joint was not asked to move, just copy the known position
                        m_commandBuilder.Append(m_knownPositions[i].ToString());
                    }
                }
                m_commandBuilder.Append(",");
            }

            if (isActualMove)
            {
                m_commandBuilder.Append($"{m_delay}\n");

                return m_commandBuilder.ToString();
            }
            else
            {
                // no actual move detected
                return null;
            }
        }
    }
}
